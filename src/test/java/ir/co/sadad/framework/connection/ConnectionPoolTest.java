package ir.co.sadad.framework.connection;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertTrue;

class ConnectionPoolTest {
    Properties properties = new Properties();
    String url ;
    String userName;
    String password ;

    @BeforeEach
    void setup(){

        this.url="jdbc:mysql://127.0.0.1:3306/SadadTest?useUnicode=true&amp;characterEncoding=UTF-8";
        this.userName = "root";
        this.password = "2326";
    }

    @Test
    void getDataSource() {

        try {
            ConnectionPool connectionPool = ConnectionPool.createConnectionPool();
            connectionPool.setUpPool(url,userName,password);
            DataSource dataSource = connectionPool.getDataSource();
            assertTrue(dataSource!=null);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }



}