package ir.co.sadad.framework.connection;

import ir.co.sadad.application.entity.User;
import ir.co.sadad.framework.exception.GeneralException;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;


@RunWith(JUnitPlatform.class)
class QueryGeneratorUserTest {



    QueryGenerator queryGenerator;


    @BeforeEach
    public void init() {
        queryGenerator = new QueryGenerator(User.class);

    }


    @Test
    public void findAllUsersQueryTest() {
        try {
            assertEquals(queryGenerator.findAll(),"SELECT * FROM User");
        } catch (GeneralException e) {
            e.printStackTrace();
        }
    }


}