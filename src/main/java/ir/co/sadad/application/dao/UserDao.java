package ir.co.sadad.application.dao;

import ir.co.sadad.application.entity.User;
import ir.co.sadad.framework.exception.GeneralException;
import ir.co.sadad.framework.manager.EntityManager;
import ir.co.sadad.framework.manager.EntityManagerFactory;
import ir.co.sadad.framework.manager.Persistence;

import java.util.List;

public class UserDao {

    EntityManager entityManager;

    public UserDao() throws GeneralException {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityMangerFactory("config.properties");
        entityManager = entityManagerFactory.createEntityManager();
    }

    public List<User> findAll() throws GeneralException {
        List<User> all = entityManager.findAll(User.class);
        return all;
    }

}
