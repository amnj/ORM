package ir.co.sadad.application.controller;

import ir.co.sadad.application.dao.UserDao;
import ir.co.sadad.application.entity.User;
import ir.co.sadad.framework.exception.GeneralException;

import java.util.List;

public class UserController {
    UserDao userDao;

    public UserController() throws GeneralException {
        userDao = new UserDao();
    }

    public List<User> getUsers() throws GeneralException {
        return userDao.findAll();
    }

}
