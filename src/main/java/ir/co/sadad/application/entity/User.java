package ir.co.sadad.application.entity;

import ir.co.sadad.framework.annotation.Column;
import ir.co.sadad.framework.annotation.Table;

@Table(name = "User")
public class User {

    @Column(name = "id")
    Long id;
    @Column(name = "user_name")
    String userName;
    @Column(name = "first_name")
    String firstName;
    @Column(name = "last_name")
    String lastName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("id : " + id + "\n") ;
        stringBuilder.append("userName : " + userName+ "\n") ;
        stringBuilder.append("firstName : " + firstName + "\n") ;
        stringBuilder.append("lastName : " + lastName + "\n") ;
        return stringBuilder.toString();
    }
}
