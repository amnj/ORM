package ir.co.sadad.run;

import ir.co.sadad.application.controller.UserController;
import ir.co.sadad.application.entity.User;
import ir.co.sadad.framework.exception.GeneralException;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        UserController userController = null;
        try {
            userController = new UserController();
        } catch (GeneralException e) {
            System.out.println(e.getMessage());
        }
        List<User> users = null;
        try {
            users = userController.getUsers();
        } catch (GeneralException e) {
            System.out.println(e.getMessage());
        }
        for (User user : users){
            System.out.println(user.toString() + "\n\n");
        }
    }

}
