package ir.co.sadad.framework.exception;

public class GeneralException extends Exception {

    public String message;
    public Exception e;

    public GeneralException(String message) {
        super(message);
        this.message = message;
    }

    public GeneralException(String message, Exception e) {
        super(message, e);
        this.message = message;
        this.e = e;
    }
}
