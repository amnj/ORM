package ir.co.sadad.framework.exception;

public enum ConstList {
    DATABASE_NOT_FOUND("Database not Found"),
    ENTITY_CLASS_NOT_FOUND("Specified class is not an ORM managed bean,Annotate it with @Table"),
    CONFIG_FILE_DOES_NOT_RECOGNIZE("Config file could not find , our fields could not bind"),
    QUERY_EXECUTION_FAILED("Query execution failed"),
    CONNECTION_CREATION_FAILED("Connection creation filed,check configs"),
    MAPPING_EERROR("Query result could not map to entity");

    private String message;

    ConstList(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
