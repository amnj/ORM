package ir.co.sadad.framework.manager;

import ir.co.sadad.framework.exception.GeneralException;

import java.util.List;

/**
 * ORM EntityManager interface
 */
public interface EntityManager {

    /**
     * Find all entities of T type
     *
     * @param clazz
     * @return List<T>
     */
    <T> List<T> findAll(Class<T> clazz) throws GeneralException;

}
