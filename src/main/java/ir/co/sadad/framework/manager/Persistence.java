package ir.co.sadad.framework.manager;

import ir.co.sadad.framework.exception.ConstList;
import ir.co.sadad.framework.exception.GeneralException;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * Persistence class
 * operate as first level factory in abstract factory pattern
 * */
public class Persistence {

    /**
     * Factory method for EntityManager factory
     * pass your database configuration and it generates EntityManagerFactory object with your settings
     * @param configName (in our scenario use property files as configuration file)
     * */
    public static EntityManagerFactory createEntityMangerFactory(String configName) throws GeneralException {
        Properties configProperties = new Properties();
        try {
            configProperties.load(new FileReader(Persistence.class.getClassLoader().getResource(configName).getFile()));
            String url = configProperties.getProperty("url");
            String userName = configProperties.getProperty("userName");
            String password = configProperties.getProperty("password");
            EntityManagerFactoryImpl entityManagerFactory = new EntityManagerFactoryImpl(url,userName,password);
            return entityManagerFactory;
        } catch (IOException e) {
            Logger.getLogger(Persistence.class.getName());
            throw new GeneralException(ConstList.CONFIG_FILE_DOES_NOT_RECOGNIZE.getMessage());
        }
    }

}
