package ir.co.sadad.framework.manager;


/**
 * ORM EntityManagerFactory interface
 */
public interface EntityManagerFactory {

    /**
     * Create entity manager
     */
    EntityManager createEntityManager();

}
