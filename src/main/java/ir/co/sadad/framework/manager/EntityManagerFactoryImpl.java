package ir.co.sadad.framework.manager;

import ir.co.sadad.framework.connection.ConnectionPool;

/**
 * Implementation of EntityManagerFactory interface
 */
public class EntityManagerFactoryImpl implements EntityManagerFactory {

    private final String url;
    private final String userName;
    private final String password;

    protected EntityManagerFactoryImpl(String url, String userName, String password) {
        this.url = url;
        this.userName = userName;
        this.password = password;
        try {
            ConnectionPool.createConnectionPool().setUpPool(url, userName, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityManager createEntityManager() {
        EntityManagerImpl entityManager = new EntityManagerImpl();
        return entityManager;
    }
}
