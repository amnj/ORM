package ir.co.sadad.framework.manager;

import ir.co.sadad.framework.connection.ConnectionManagerFactory;
import ir.co.sadad.framework.connection.QueryGenerator;
import ir.co.sadad.framework.exception.ConstList;
import ir.co.sadad.framework.exception.GeneralException;
import ir.co.sadad.framework.mapper.EntityMapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;

/**
 * Implementation of EntityManager interface
 */
public class EntityManagerImpl implements EntityManager {

    private Class clazz;

    public Class getClazz() {
        return clazz;
    }

    public void setClazz(Class clazz) {
        this.clazz = clazz;
    }

    /**
     * {@inheritDoc}
     */

    @Override
    public <T> List<T> findAll(Class<T> clazz) throws GeneralException {

        Connection connection = ConnectionManagerFactory.createConnectionManager().getConnection();
        PreparedStatement pstmt = null;
        try {
            pstmt = connection.prepareStatement(new QueryGenerator(clazz).findAll());
        } catch (SQLException e) {
            Logger.getLogger(EntityManager.class.getName()).severe(e.getMessage());
            throw new GeneralException(ConstList.QUERY_EXECUTION_FAILED.getMessage());
        }
        ResultSet resultSet = null;
        List<T> tList = null;
        try {

            resultSet = pstmt.executeQuery();
            tList = EntityMapper.mapResultSetToEntities(resultSet, clazz);
        } catch (SQLException e) {
            Logger.getLogger(EntityManager.class.getName()).severe(e.getMessage());
            throw new GeneralException(ConstList.QUERY_EXECUTION_FAILED.getMessage());
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                Logger.getLogger(EntityManager.class.getName()).severe(e.getMessage());
            }
        }
        return tList;
    }
}
