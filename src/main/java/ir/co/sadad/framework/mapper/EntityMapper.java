package ir.co.sadad.framework.mapper;

import ir.co.sadad.framework.annotation.Column;
import ir.co.sadad.framework.exception.ConstList;
import ir.co.sadad.framework.exception.GeneralException;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Mapper class for mapping sql resultSets to Java beans
 * */
public class EntityMapper {

    /**
     * at DataAccess level we used PreparedStatement and return a ResultSet as out query result
     * this method writes to consume that ResultSet to map each record to an entity , using Entity class annotations
     * @param resultSet
     * @param tClass
     * @return List<T>
     * */
    public static <T> List<T> mapResultSetToEntities(ResultSet resultSet , Class<T> tClass) throws GeneralException {
        try {
            List<T> tList = new ArrayList<>();
            resultSet.beforeFirst();
            while (resultSet.next()){
                T t = tClass.newInstance();
                for (int i = 1 ; i<=resultSet.getMetaData().getColumnCount();i++)
                    t = fillObject(resultSet.getMetaData().getColumnName(i),resultSet.getObject(i),tClass,t);
                tList.add(t);
            }
            return tList;
        } catch (SQLException e) {
            Logger.getLogger(EntityMapper.class.getName()).severe(e.getMessage());
            throw new GeneralException(ConstList.MAPPING_EERROR.getMessage());
        } catch (IllegalAccessException e) {
            Logger.getLogger(EntityMapper.class.getName()).severe(e.getMessage());
            throw new GeneralException(ConstList.MAPPING_EERROR.getMessage());
        } catch (InstantiationException e) {
            Logger.getLogger(EntityMapper.class.getName()).severe(e.getMessage());
            throw new GeneralException(ConstList.MAPPING_EERROR.getMessage());
        }
    }

    /**
     * Filling object fields using reflection and ResultSet columns value
     * @param columnName
     * @param value
     * @param aClass
     * @param obj
     * @return T
     * */
    public static <T> T fillObject(String columnName ,Object value, Class<T> aClass ,Object obj) throws GeneralException {
        Field[] fields = aClass.getDeclaredFields();
        String fieldName =null ;
        for (Field field : fields){
            field.setAccessible(true);
            if (field.isAnnotationPresent(Column.class)&&field.getAnnotation(Column.class).name().equalsIgnoreCase(columnName)){
                try {
                    field.set(obj , value);
                } catch (IllegalAccessException e) {
                    Logger.getLogger(EntityMapper.class.getName()).severe(e.getMessage());
                    throw new GeneralException(ConstList.MAPPING_EERROR.getMessage());
                }
            }
        }
        return (T) obj;
    }

}
