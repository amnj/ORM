package ir.co.sadad.framework.repository;

import ir.co.sadad.framework.connection.QueryGenerator;

/**
 * Repository of ORM methods
 * */
public class Repository {

    public String findAll() throws Exception {
        return new QueryGenerator.Builder().select().star().from().generateQuery();
    }

}
