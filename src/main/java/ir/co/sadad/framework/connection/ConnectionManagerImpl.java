package ir.co.sadad.framework.connection;

import ir.co.sadad.framework.exception.ConstList;
import ir.co.sadad.framework.exception.GeneralException;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Logger;

/**
 * Implementation of ConnectionManager interface
 * */
public class ConnectionManagerImpl implements ConnectionManager {

    /**{@inheritDoc}*/

    @Override
    public Connection getConnection() throws GeneralException {
        DataSource dataSource = ConnectionPool.createConnectionPool().getDataSource();
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            Logger.getLogger(getClass().getName()).severe(e.getMessage());
            throw new GeneralException(ConstList.CONNECTION_CREATION_FAILED.getMessage());
        }
    }

}
