package ir.co.sadad.framework.connection;

import ir.co.sadad.framework.exception.GeneralException;

import java.sql.Connection;

/**
 * Connection manager class
 * */
public interface ConnectionManager {

    /** Get connection from connection pool
     * @return  Connection
     * */
    Connection getConnection() throws GeneralException;
}
