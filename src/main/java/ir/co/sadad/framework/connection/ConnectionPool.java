package ir.co.sadad.framework.connection;

import org.apache.commons.dbcp.BasicDataSource;

import javax.sql.DataSource;

/**
 * Connection pooling using dbcp library
 * */
//Singleton
public class ConnectionPool {

    private static final ConnectionPool connectionPool= new ConnectionPool();

    public static ConnectionPool createConnectionPool(){
        return connectionPool ;
    }


    private BasicDataSource basicDataSource;

    private ConnectionPool(){

    }

    /***
     * Setting up connection pool for EntityManager
     * @param url
     * @param userName
     * @param password
     */
    public void setUpPool(String url,String userName,String password) throws Exception {
        basicDataSource = new BasicDataSource();
        basicDataSource.setDriverClassName("com.mysql.jdbc.Driver");
        basicDataSource.setUsername(userName);
        basicDataSource.setPassword(password);
        basicDataSource.setUrl(url);
        // the settings below are optional -- dbcp can work with defaults
        basicDataSource.setMinIdle(5);
        basicDataSource.setMaxIdle(20);
        basicDataSource.setMaxOpenPreparedStatements(180);

    }

    public DataSource getDataSource() {
        return basicDataSource;
    }

}
