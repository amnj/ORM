package ir.co.sadad.framework.connection;

import ir.co.sadad.framework.annotation.Table;
import ir.co.sadad.framework.exception.ConstList;
import ir.co.sadad.framework.exception.GeneralException;

import java.util.logging.Logger;

/**
 * This class works as a CriteriaBuilder
 * implements in a simple way , very simple way
 * */
public class QueryGenerator {

    static Class clazz;

    public QueryGenerator(Class clazz) {
        this.clazz = clazz;
    }

    public String findAll() throws GeneralException {
        return new Builder().select().star().from().generateQuery();
    }

    /***
     * Query builder class , this class build a sql query with java methods
     */
    public static class Builder {
        private StringBuilder query = new StringBuilder();

        public Builder select() {
            query.append("SELECT ");
            return this;
        }

        public Builder star() {
            query.append("*");
            return this;
        }

        public Builder from() throws GeneralException {
            query.append("FROM ");
            query.append(retrieveTableName());
            return this;
        }

        public String generateQuery() {
            return query.toString();
        }
    }

    /**
     * when this method calls ,the class that has been passed to this constructor method , being check ,
     * and if annotated with @Table , the value of its annotation will retrieve to use it for object mapping .
     * */
    public static String retrieveTableName() throws GeneralException {
        if (clazz.isAnnotationPresent(Table.class)) {
            Table table= (Table) clazz.getDeclaredAnnotation(Table.class);
            String name = table.name();
            return name;
        }else {
            Logger.getLogger(QueryGenerator.class.getName()).severe(ConstList.ENTITY_CLASS_NOT_FOUND.getMessage());
            throw new GeneralException(ConstList.ENTITY_CLASS_NOT_FOUND.getMessage());
        }
    }

}
