package ir.co.sadad.framework.connection;

/**
 * Create ConnectionManager object
 * */
public class ConnectionManagerFactory {
    /***
     * Factory method for ConnectionManager implementations
     * @return ConnectionManager
     */
    public static ConnectionManager createConnectionManager(){
        return new ConnectionManagerImpl();
    }

}
